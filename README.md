# Useful Info

Handy information and links for members of the Support team.

Published as a static site: https://gitlab-com.gitlab.io/support/useful-info

## Why?

Information is scattered around the handbook and the various services we use. 
I have a bunch of bookmarks and it can take some time to find what I need.
I want a 'landing page' where all the most commonly needed pieces
of information are collected together in a sensible order.

This page can be bookmarked / kept as a pinned tab to help find what I'm looking for more quickly.
You might want to try it too!

## Status - it's unofficial WIP

**Why not in the handbook?** This is an unoffical MVC started by Tom A to try out a few things.

It would be helpful to make API calls to various support services (e.g. PagerDuty) to populate
the page with dynamic information. That would mean running these API calls every time the handbook CI job
runs (100s of times a day)! If it becomes clear that this should be a handbook page we can transfer
it there if we can get all the features to work while not slowing down generation of the handbook.

The idea is not to duplicate info already in the handbook. Instead this will link to relevant info
and grab dynamic info (like who is on-call) so that it's a 'one stop shop' to get your bearings
while working in Support.

So this _could_ stay as an independent project - with a link from the handbook of course!

## Team members only

Currently the GitLab Pages site is set to 'Team members only'. That means it's safe to publish
internal only links etc - a bit like SWIR is 'team members only'.

## Want to contribute?

Feel free to submit issues or MRs to make this better, or ping Tom A with your ideas.
