---
title: GitLab Support Team Useful Info
---

This is a condensed version of the information on the [Support Handbook home page](https://about.gitlab.com/handbook/support/) for easier navigation and with some extra 'internal only' links. Currently it's [unofficial WIP by Tom A](https://gitlab.com/gitlab-com/support/useful-info/blob/master/README.md).

Want to add or change something? [Create an issue](https://gitlab.com/gitlab-com/support/useful-info/issues) or [edit this page](https://gitlab.com/gitlab-com/support/useful-info/blob/master/source/index.html.md) :-)

## Support Week in Review

Please add things to [Support Week in Review](https://docs.google.com/document/d/1eyMzbzImSKNFMpmu33C6imvC1iWEWHREJqaD6mkVDNg/edit#heading=h.wscvixurw4vb){:target="_blank"} that everyone on the Support Team should know about.  
Updated every week and finalised on a Friday. Essential reading each Monday morning!

## Support Engineer Responsibilities

[Support Engineer Responsibilities](https://about.gitlab.com/handbook/support/support-engineer-responsibilities.html){:target="_blank"} - important guidance on what to focus on and how.

## Support Rotations - who's on call?

[On call schedules](https://gitlab-com.gitlab.io/support/team/oncall.html){:target="_blank"}

## Support Pairing

[Create issues here](https://gitlab.com/gitlab-com/support/support-pairing/){:target="_blank"} whenever you take part in a pairing session.
Try and be part of at least two pairings a week.

## Support Training

Develop your skills by completing modules from the [Support Training project](https://gitlab.com/gitlab-com/support/support-training/){:target="_blank"}

## Make Support better

### Support Epics

[Support Epics](https://gitlab.com/groups/gitlab-com/support/-/epics){:target="_blank"} are groups of issues for larger team initiatives. Follow areas you're interested in and volunteer to help!

### Support Meta

Make Suppport better by [contributing to issues in the Support Meta project](https://gitlab.com/gitlab-com/support/support-team-meta/issues){:target="_blank"}.

## Handy Links

### Support Specific

- [Support Handbook Home](https://about.gitlab.com/handbook/support/){:target="_blank"} - more detailed versions of the info on this page.
= [Support subgroup](https://gitlab.com/groups/gitlab-com/support/){:target="_blank"} -  for all our projects
- [Support Key Performance Indicators (KPIs)](https://about.gitlab.com/handbook/support/performance-indicators/){:target="_blank"} - how are we doing?
- [Support Shared Drive](https://drive.google.com/drive/folders/0AEeARMMpt4eDUk9PVA){:target="_blank"} - Google Drive for Support resources not in handbook
- [Definitions of Priority Support and Customer SLAs](https://about.gitlab.com/support/#priority-support){:target="_blank"} - what we offer to our customers
- [Customer Support Portal](https://support.gitlab.com/hc/en-us){:target="_blank"} - where customers submit tickets
- [Internal Support for GitLab Team Members](https://about.gitlab.com/handbook/support/internal-support/){:target="_blank"} - handy page to share with other team members looking for info on how to interact with us

### General GitLab

- [GitLab changelog](https://gitlab.com/gitlab-org/gitlab/blob/master/CHANGELOG.md){:target="_blank"} - full changelog for our main application.
- [Product stages, groups, and categories](https://about.gitlab.com/handbook/product/categories/){:target="_blank"} with Product Managers, Engineering Managers etc
- [Product category maturity and direction](https://about.gitlab.com/direction/maturity/){:target="_blank"} - direction for GitLab
- [Customer account projects](https://gitlab.com/gitlab-com/account-management){:target="_blank"} created by Customer Success for larger customers. Contains architecture diagrams and issues shared with customers.
- [Solutions Architects issues for customer POCs etc](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards){:target="_blank"} Projects with issues where you can view SA work with customers
- [Geo Customer issues](https://gitlab.com/gitlab-com/geo-customers/issues){:target="_blank"} - the Geo team will answer Geo questions about specific customers here

## Our team

**Want a quick way to see who is in the team by region? Team page taking too long to load?** The [Org Chart](https://comp-calculator.gitlab.net/org_chart){:target="_blank"} is useful for seeing team structure.

**Want to see people's faces? Team page too slow?** The links below go to the super fast '[Better Team page](https://leipert-projects.gitlab.io/better-team-page/){:target="_blank"}'

### Managers

- [Reports to Tom Cooney](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:tom-cooney){:target="_blank"} (VP)

### APAC

- [Reports to Shaun McCann](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:shaun-mccann){:target="_blank"} (Senior Manager)
- [Reports to Jane Gianoutsos](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:janegianoutsos){:target="_blank"}
- [Reports to Wei-Meng Lee](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:wei-meng){:target="_blank"}
- [Reports to Viji Rao](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:viji){:target="_blank"}

### EMEA

- [Reports to Val Parsons](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:valparsons){:target="_blank"} (Director)
- [Reports to Tom Atkins](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:tom-atkins){:target="_blank"}
- [Reports to Ilia Kosenko](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:Ilia-kosenko){:target="_blank"}
- [Reports to John Lyttle](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:johnlyttle){:target="_blank"}
- [Reports to Tine Sørensen](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:tinesørensen){:target="_blank"}
- [Reports to Rebecca Spainhower](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:rebecca-spainhower){:target="_blank"}

### AMER

- [Reports to Lee Matos](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:lee-m){:target="_blank"} (Senior Manager)
- [Reports to Lyle Kozloff](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:lyle){:target="_blank"} (Senior Manager)
- [Reports to Jason Colyer](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:jason-colyer){:target="_blank"} (Support Ops)
- [Reports to Ronnie Alfaro](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:ronnie-alfaro){:target="_blank"}
- [Reports to Izzy Fee](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:izzyfee){:target="_blank"}
- [Reports to James Lopes](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:jameslopes){:target="_blank"}
- [Reports to Aric Buerer](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:aric-b){:target="_blank"}
- [Reports to Mike Dunninger](https://leipert-projects.gitlab.io/better-team-page/?search=reports_to:michael-dunninger){:target="_blank"}


### Support subgroups for @mentioning a region

- [Support APAC](https://gitlab.com/groups/gitlab-com/support/apac/-/group_members?with_inherited_permissions=exclude){:target="_blank"}
- [Support EMEA](https://gitlab.com/groups/gitlab-com/support/emea/-/group_members?with_inherited_permissions=exclude){:target="_blank"}
- [Support AMER](https://gitlab.com/groups/gitlab-com/support/amer/-/group_members?with_inherited_permissions=exclude){:target="_blank"}

Someone missing? Add them as an 'Owner' to the relevant group.

## Make this page better!

[Create an issue](https://gitlab.com/gitlab-com/support/useful-info/issues) if you'd like to contribute :-)
